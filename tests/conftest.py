#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
import pytest

from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework.test import APIClient
import rest_framework.status as status
from typing import Any, Dict, List

from spacenews_api.models import Event, Category, Click  # noqa


def base_url(
    host=os.environ.get("API_HOST", "localhost"),
    port=os.environ.get("API_HTTP_PORT", 8000),
    base_path="api/v1.0/",
) -> str:
    """base URL for testing."""
    return f"http://{host}:{port}/{base_path}"


def get_endpoint(endpoint: str, **kwargs) -> str:
    """Get a full endpoint on top of the base URL."""
    return f"{base_url(**kwargs)}{endpoint}"


@pytest.fixture(scope="function")
def event_data() -> Dict[str, Any]:
    """Event data fixture."""
    return {
        "ext_id": "EONET_4528",
        "source": "EONET",
        "date": "2019-12-26",
        "title": "Wildfire - Paia, Maui, HI, United States ",
        "url": "https://place.somewhere.com",
    }


@pytest.fixture(scope="function")
def event_obj(event_data: Dict[str, Any]) -> Event:
    """Event obj fixture."""
    ev = Event(**event_data)
    ev.save()
    return ev


@pytest.fixture(scope="function")
def category_data(event_obj: Event) -> Dict[str, Any]:
    """Category data fixture."""
    return {"title": "Wildires", "event": event_obj.id}


@pytest.fixture(scope="function")
def user_data(event_obj: Event) -> Dict[str, Any]:
    """User data fixture."""
    return {"email": "foo@bar.com", "password": "s3cr3t!" * 3}


@pytest.fixture(scope="function")
def user_obj(user_data: Dict[str, Any]) -> User:
    """User obj fixture."""
    return User.objects.create_user(
        username=user_data["email"],
        email=user_data["email"],
        password=user_data["password"],
    )


@pytest.fixture(scope="function")
def click_data(event_obj: Event, user_obj: User) -> Dict[str, Any]:
    """Click data fixture."""
    return {"user": user_obj.id, "event": event_obj.id}


@pytest.fixture(scope="function")
def sign_up_user(user_data: Dict[str, Any]) -> Dict[str, Any]:
    """Sign up new User."""
    endpoint = get_endpoint("sign-up/")
    requests = APIClient()
    data = user_data
    res: Response = requests.post(
        endpoint, json.dumps(data), content_type="application/json"
    )
    assert res.status_code == status.HTTP_201_CREATED
    return res.data


@pytest.fixture(scope="function")
def eonet_events() -> List[Any]:
    events = [
        {
            "categories": [{"id": 8, "title": "Wildfires"}],
            "description": "",
            "geometries": [
                {
                    "coordinates": [-156.36924, 20.90311],
                    "date": "2019-12-14T21:22:00Z",
                    "type": "Point",
                }
            ],
            "id": "EONET_4528",
            "link": "https://eonet.sci.gsfc.nasa.gov/api/v2.1/events/EONET_4528",
            "sources": [
                {"id": "PDC", "url": "http://emops.pdc.org/emops/?hazard_id=99213"}
            ],
            "title": "Wildfire - Paia, Maui, HI, United States ",
        },
        {
            "categories": [{"id": 8, "title": "Wildfires"}],
            "description": "",
            "geometries": [
                {
                    "coordinates": [-70.437236929, -33.560330351],
                    "date": "2019-12-13T16:37:00Z",
                    "type": "Point",
                }
            ],
            "id": "EONET_4525",
            "link": "https://eonet.sci.gsfc.nasa.gov/api/v2.1/events/EONET_4525",
            "sources": [
                {"id": "PDC", "url": "http://emops.pdc.org/emops/?hazard_id=99149"}
            ],
            "title": "Wildfires - Litueche Municipality, Chile",
        },
        {
            "categories": [{"id": 8, "title": "Wildfires"}],
            "description": "",
            "geometries": [
                {
                    "coordinates": [-71.90292, -34.64856],
                    "date": "2019-12-13T16:35:00Z",
                    "type": "Point",
                }
            ],
            "id": "EONET_4526",
            "link": "https://eonet.sci.gsfc.nasa.gov/api/v2.1/events/EONET_4526",
            "sources": [
                {"id": "PDC", "url": "http://emops.pdc.org/emops/?hazard_id=99147"}
            ],
            "title": "Wildfires - Paredones Commune, Chile",
        },
    ]
    return events


@pytest.fixture(scope="function")
def requester(eonet_events: List[Any]) -> object:
    class Req(object):

        """docstring for Req. """

        def __init__(self) -> None:
            """Constructor of Req."""
            pass

        def list_events(self) -> List[any]:
            return eonet_events

    return Req()
