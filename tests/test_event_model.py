#!/usr/bin/env python
# -*- coding: utf-8 -*-


import pytest

from typing import Any, Dict
from spacenews_api.models.event import EventSerializer


class TestEventSerializer:

    """TestEventSerializer"""

    @pytest.mark.django_db
    def test_ser_data_save(self, event_data: Dict[str, Any]) -> None:
        """Test serialization data save."""
        serd = EventSerializer(data=event_data)
        assert serd.is_valid()
        for field in ("ext_id", "title"):
            assert field in serd.validated_data
        serd.save()
