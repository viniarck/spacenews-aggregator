#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import Dict

import pytest
from django.contrib.auth.models import User
import rest_framework.status as status
from rest_framework.response import Response
from rest_framework.test import APIClient

from conftest import get_endpoint
from spacenews_api.models import Event, Click


class TestTopEvents(object):
    """TestTopEvents. """

    @pytest.mark.django_db
    def test_top_events(
        self, sign_up_user: Dict[str, str], user_data: Dict[str, str]
    ) -> None:
        """Test top events."""

        user = User.objects.get(email=user_data["email"])
        events = [("one", 1, "EONET"), ("two", 2, "EONET"), ("three", 3, "EONET")]
        for ev in events:
            event = Event(title=ev[0], ext_id=ev[1], source=ev[2])
            event.save()
            for _ in range(ev[1]):
                Click(event=event, user=user).save()
        data = sign_up_user
        endpoint = get_endpoint("events-top/?limit=10")
        requests = APIClient()
        requests.credentials(HTTP_AUTHORIZATION=f"Bearer {data['token']}")
        res: Response = requests.get(endpoint, content_type="application/json")
        assert res.status_code == status.HTTP_200_OK
        assert res.data[0]["title"] == "three"
        assert res.data[0]["clicks"] == 3
        assert res.data[1]["clicks"] == 2
