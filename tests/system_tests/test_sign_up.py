#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import Dict

import pytest
import rest_framework.status as status
from rest_framework.response import Response
from rest_framework.test import APIClient

import json
from spacenews_api.models.user import User
from spacenews_api.views.auth import JWTAPIView
from conftest import get_endpoint


class TestSignUpView(object):
    """TestSignUpView. """

    @pytest.mark.django_db
    def test_create_login(self, user_data: Dict[str, str]) -> None:
        """Test create login."""
        endpoint = get_endpoint("sign-up/")
        requests = APIClient()
        data = user_data
        res: Response = requests.post(
            endpoint, json.dumps(data), content_type="application/json"
        )
        assert res.status_code == status.HTTP_201_CREATED
        user: User = User.objects.get(email=data["email"])
        token = JWTAPIView.get_token(user)
        # only assert the first segment of the token since it has to be the same
        assert res.data["token"].split(".")[0] == token.split(".")[0]
