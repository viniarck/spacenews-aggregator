#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import Dict

import pytest
import rest_framework.status as status
from rest_framework.response import Response
from rest_framework.test import APIClient

import json
from conftest import get_endpoint


class TestSignInView(object):
    """TestSignInView. """

    @pytest.mark.django_db
    def test_sign_in_login(self, user_data: Dict[str, str]) -> None:
        """Test sign_in login."""
        endpoint = get_endpoint("sign-up/")
        requests = APIClient()
        data = user_data
        res: Response = requests.post(
            endpoint, json.dumps(data), content_type="application/json"
        )
        assert res.status_code == status.HTTP_201_CREATED

        endpoint = get_endpoint("sign-in/")
        res = requests.post(
            endpoint, json.dumps(data), content_type="application/json"
        )
        assert res.status_code == status.HTTP_201_CREATED
