#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import Dict

import pytest
import rest_framework.status as status
from rest_framework.response import Response
from rest_framework.test import APIClient

from conftest import get_endpoint


class TestEvents(object):
    """TestEvents. """

    @pytest.mark.django_db
    def test_list_events(
        self,
        monkeypatch,
        requester,
        sign_up_user: Dict[str, str],
        user_data: Dict[str, str],
    ) -> None:
        """Test list events."""
        monkeypatch.setattr("spacenews_api.requester.Requester", requester)
        monkeypatch.setattr("spacenews_api.views.events.EventsView.req", requester)
        data = sign_up_user
        endpoint = get_endpoint("events/")
        requests = APIClient()
        requests.credentials(HTTP_AUTHORIZATION=f"Bearer {data['token']}")
        res: Response = requests.get(endpoint, content_type="application/json")
        assert res.status_code == status.HTTP_200_OK
        assert len(res.data) == 3
