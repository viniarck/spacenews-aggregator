#!/usr/bin/env python
# -*- coding: utf-8 -*-


import pytest

from typing import Any, Dict
from spacenews_api.models.category import CategorySerializer


class TestCategorySerializer:

    """TestCategorySerializer"""

    @pytest.mark.django_db
    def test_ser_data_save(
        self, category_data: Dict[str, Any], event_data: Dict[str, Any]
    ) -> None:
        """Test serialization data save."""
        serd = CategorySerializer(data=category_data)
        assert serd.is_valid()
        for field in ("title", "event"):
            assert field in serd.validated_data
        cat = serd.save()
        assert cat.event.title == event_data["title"]
