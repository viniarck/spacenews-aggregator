#!/usr/bin/env python
# -*- coding: utf-8 -*-


import pytest

from typing import Any, Dict
from spacenews_api.models.click import ClickSerializer


class TestClickSerializer:

    """TestClickSerializer"""

    @pytest.mark.django_db
    def test_ser_data_save(
        self,
        click_data: Dict[str, Any],
        event_data: Dict[str, Any],
        user_data: Dict[str, Any],
    ) -> None:
        """Test serialization data save."""
        serd = ClickSerializer(data=click_data)
        assert serd.is_valid()
        for field in ("user", "event"):
            assert field in serd.validated_data
        click = serd.save()
        assert click.user.email == user_data["email"]
        assert click.event.title == event_data["title"]
