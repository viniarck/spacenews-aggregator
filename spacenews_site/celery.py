#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Celery app instance."""

import logging
import os

from celery import Celery
from celery.signals import setup_logging

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "spacenews_site.settings")
app = Celery("spacenews_site")

logger = logging.getLogger(__name__)

app.config_from_object("django.conf:settings", namespace="CELERY")
app.conf.result_expires = 60 * 60 * 24  # in seconds
app.autodiscover_tasks()
app.conf.beat_schedule = {
    "fetch_eonet_events": {
        "task": "spacenews_api.tasks.fetch_eonet_events",
        "schedule": 60 * 10,  # in seconds
    }
}
app.conf.timezone = "UTC"


@setup_logging.connect
def config_loggers(*args, **kwags):
    from logging.config import dictConfig
    from django.conf import settings

    dictConfig(settings.CUSTOM_LOGGING_CONFIG)
