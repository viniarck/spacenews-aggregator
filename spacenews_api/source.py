#!/usr/bin/env python
# -*- coding: utf-8 -*-

from abc import abstractmethod
from typing import Any, Dict, List

from spacenews_api.models import Event


class Source:

    """Represents an Event Source. Creation and Distribution."""

    def __init__(self, name: str) -> None:
        """Constructor of Source."""
        self.name = name

    @abstractmethod
    def create_event(
        self, event: Dict[str, Any], *events: List[Dict[str, Any]]
    ) -> List[Event]:
        """Create an Event. If creating multiple events, wrap this call with
        django.db.transaction or bulk_create"""
        pass

    @abstractmethod
    def list_events(self, *args, **kwargs) -> List[Dict[str, Any]]:
        """List events."""
        pass
