#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
spacenews URLs.
"""
from django.urls import path
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions

from spacenews_api.views.click import ClickView
from spacenews_api.views.sign_up import SignUpView
from spacenews_api.views.sign_in import SignInView
from spacenews_api.views.events import EventsView
from spacenews_api.views.top_events import TopEventsView
from spacenews_api.views.recommend_events import RecommendEventsView

schema_view = get_schema_view(
    openapi.Info(
        title="spacenews-api",
        default_version="v1",
        description="spacenews-api",
        contact=openapi.Contact(email="viniarck@gmail.com"),
        license=openapi.License(name="Apache"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path(
        "doc/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path("doc/spec/", schema_view.without_ui(cache_timeout=0), name="schema-json"),
    path("sign-up/", SignUpView.as_view()),
    path("sign-in/", SignInView.as_view()),
    path("events/", EventsView.as_view()),
    path("events-top/", TopEventsView.as_view()),
    path("events-recommend/<str:email>/", RecommendEventsView.as_view()),
    path("click/", ClickView.as_view()),
]
