#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Exceptions."""

from rest_framework.exceptions import APIException


class Http400(APIException):
    """Represent a HTTP 400 Error."""

    status_code = 400


class Http401(APIException):
    """Represent a HTTP 401 Error."""

    status_code = 401


class Http403(APIException):
    """Represent a HTTP 403 Error."""

    status_code = 403


class Http404(APIException):
    """Represent a HTTP 404 Error."""

    status_code = 404


class Http409(APIException):
    """Represent a HTTP 409 Error."""

    status_code = 409
