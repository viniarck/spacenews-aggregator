#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Celery tasks."""

import os

import django
import logging
from celery import shared_task
from spacenews_api.requester import Requester


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "spacenews_site.settings")
django.setup()

logger = logging.getLogger(__name__)
req = Requester()


@shared_task(
    bind=True,
    autoretry_for=(Exception,),
    default_retry_delay=20,
    max_retries=3,
)
def fetch_eonet_events(self) -> None:
    """Fetch eonet events."""
    req._fetch_update()
