#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
import logging
from functools import reduce
from django.db.models import Count, Q
from drf_yasg.utils import swagger_auto_schema
import rest_framework.status as status
from rest_framework.response import Response
from rest_framework.request import Request
from typing import Dict

from spacenews_api.models.category import Category
from spacenews_api.models.event import Event, EventSerializer
from spacenews_api.models.click import Click
from spacenews_api.exceptions import Http400
from spacenews_api.views.auth import JWTAPIView

logger = logging.getLogger(__name__)


class RecommendEventsView(JWTAPIView):

    """RecommendEventsView."""

    @swagger_auto_schema(responses={200: EventSerializer(many=True)})
    def get(self, request: Request, email: str) -> Response:
        """List Recommended events given an User's email. The recommendation algorithm factors in User's clicks, similar Event categories and random events."""  # noqa

        def fill_up_random_events(events: Dict[int, int], len_left: int) -> None:
            """Fill up events_category with random Events to add white noise
            on the recommended list."""
            evs = Event.objects.all()
            evs_len = len(evs)
            if evs and evs_len >= len_left:
                while len(events) != limit:
                    ev = evs[random.randint(0, evs_len - 1)]
                    if events.get(ev.id):
                        continue
                    events[ev.id] = ev.id

        def fill_up_similiar_events(
            events: Dict[int, int], category_title: str, similar_title_limit: int
        ) -> None:
            """Fill up events_category with similar category events."""
            events_category = (
                Category.objects.filter(title=category_title)
                .values("event_id")
                .annotate(total=Count("event_id"))
                .order_by("-total")[: similar_title_limit * 5]
            )
            if len(events_category) >= similar_title_limit:
                while len(events) != similar_title_limit:
                    ev = events_category[random.randint(0, len(events_category) - 1)]
                    if events.get(ev["event_id"]):
                        continue
                    events[ev["event_id"]] = ev["event_id"]

        def fill_up_most_clicked_events(
            events: Dict[int, int], categories_limit: int
        ) -> None:
            """Fill up with most clicked Events."""
            for click in (
                Click.objects.all()
                .values("event_id")
                .annotate(total=Count("event_id"))
                .order_by("-total")[:categories_limit]
            ):
                events[click["event_id"]] = click["event_id"]

        def most_clicked_event_by_user(email: str) -> Click:
            """Get most clicked Event given an email (user)."""
            return (
                Click.objects.filter(user__email=email)
                .values("event_id")
                .annotate(total=Count("event_id"))
                .order_by("-total", "-created")[:1]
            )

        try:
            limit = int(request.query_params.get("limit", 10))
        except ValueError as e:
            raise Http400(str(e))
        if limit < 1 or limit > 10:
            raise Http400(f"The limit query arg has to be >= 1 and <= 10")

        events: Dict[int, int] = {}
        most_clicked_event = most_clicked_event_by_user(email)
        categories_limit = limit // 2
        similar_title_limit = limit - categories_limit

        if not most_clicked_event:
            fill_up_most_clicked_events(events, categories_limit)
            logger.info(f"Filled up with most clicked {len(events)} events")
        else:
            fill_up_similiar_events(
                events,
                Category.objects.get(event_id=most_clicked_event[0]["event_id"]).title,
                similar_title_limit,
            )
            logger.info(f"Filled up with similar {len(events)} events")

        len_left = limit - len(events)
        if len_left:
            fill_up_random_events(events, len_left)
            logger.info(f"Filled up random {len_left} events")

        qs = [Q(id=v) for _, v in events.items()]
        event_serd = EventSerializer(
            Event.objects.filter(reduce(lambda x, y: x | y, qs)), many=True
        ).data
        return Response(event_serd, status=status.HTTP_200_OK)
