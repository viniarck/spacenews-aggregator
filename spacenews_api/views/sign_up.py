#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework.views import APIView
import rest_framework.status as status
from rest_framework.response import Response
from rest_framework.request import Request
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from drf_yasg.utils import swagger_auto_schema

from spacenews_api.views.auth import JWTAPIView
from spacenews_api.utils import json_or_raise
from spacenews_api.models.user import (
    UserSerializer,
    UserEmailSerializer,
    UserSignInSerializer,
)


class SignUpView(APIView):

    """SignUpView."""

    @swagger_auto_schema(
        request_body=UserSerializer, responses={201: UserSignInSerializer()}
    )
    def post(self, request: Request) -> Response:
        """Post a new User."""
        data = json_or_raise(request)
        user_serd = UserSerializer(data=data)
        if not user_serd.is_valid():
            return Response(user_serd.errors, status=status.HTTP_400_BAD_REQUEST)
        try:
            user_dict = dict(user_serd.data)
            user_dict["username"] = user_dict.get("email")
            user = User.objects.create_user(**user_dict)
        except (TypeError, ValidationError) as e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
        except IntegrityError:
            return Response(
                {"email": [f"'{user_serd.data['email']}' is already registered"]},
                status=status.HTTP_400_BAD_REQUEST,
            )
        return Response(
            {
                "token": JWTAPIView.get_token(user),
                "user": UserEmailSerializer(user).data,
            },
            status=status.HTTP_201_CREATED,
        )
