#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import rest_framework.status as status
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response
from rest_framework.request import Request
from requests.exceptions import ConnectionError, Timeout

from spacenews_api.exceptions import Http400
from spacenews_api.models.event import EventSerializer
from spacenews_api.requester import Requester
from spacenews_api.views.auth import JWTAPIView


logger = logging.getLogger(__name__)


class EventsView(JWTAPIView):

    """Events."""

    req = Requester()

    @swagger_auto_schema(responses={200: EventSerializer(many=True)})
    def get(self, request: Request) -> Response:
        """List events."""
        try:
            # TODO better pagination in a future release
            limit = int(request.query_params.get("limit", 100000))
            offset = int(request.query_params.get("offset", 0))
            if offset < 0 or limit < 0:
                raise Http400(f"offset and offset should be >=0")
        except ValueError as e:
            raise Http400(str(e))
        try:
            return Response(
                self.req.list_events(offset, limit), status=status.HTTP_200_OK
            )
        except (ConnectionError, Timeout) as e:
            logger.error(str(e))
            return Response([], status=status.HTTP_503_SERVICE_UNAVAILABLE)
