#!/usr/bin/env python
# -*- coding: utf-8 -*-

from drf_yasg.utils import swagger_auto_schema
import rest_framework.status as status
from rest_framework.response import Response
from rest_framework.request import Request


from spacenews_api.views.auth import JWTAPIView
from spacenews_api.models.user import User
from spacenews_api.models.event import Event
from spacenews_api.models.click import Click, ClickSerializer, ClickInSerializer
from spacenews_api.utils import json_or_raise
from spacenews_api.exceptions import Http400, Http404


class ClickView(JWTAPIView):

    """ClickView."""

    @swagger_auto_schema(
        request_body=ClickInSerializer, responses={201: ClickSerializer()}
    )
    def post(self, request: Request) -> Response:
        """Submit a user Click."""
        data = json_or_raise(request)
        click_serd = ClickInSerializer(data=data)
        if not click_serd.is_valid():
            raise Http400(click_serd.errors)
        data = click_serd.data
        user = User.objects.filter(email=data["email"]).first()
        if not user:
            raise Http404(f"User {data['email']} not found")
        event = Event.objects.filter(id=data["event"]).first()
        if not event:
            raise Http404(f"Event {data['event']} not found")
        click = Click(user=user, event=event)
        click.save()
        return Response(ClickSerializer(click).data, status=status.HTTP_201_CREATED)
