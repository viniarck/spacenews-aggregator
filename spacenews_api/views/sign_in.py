#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rest_framework.status as status
from django.contrib.auth.hashers import check_password
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.request import Request


from spacenews_api.views.auth import JWTAPIView
from spacenews_api.models.user import (
    User,
    UserSerializer,
    UserEmailSerializer,
    UserSignInSerializer,
)
from spacenews_api.utils import json_or_raise
from spacenews_api.exceptions import Http400, Http404


class SignInView(APIView):

    """SignInView."""

    @swagger_auto_schema(
        request_body=UserSerializer, responses={201: UserSignInSerializer()}
    )
    def post(self, request: Request) -> Response:
        """Sign-in."""
        data = json_or_raise(request)
        user_serd = UserSerializer(data=data)
        if not user_serd.is_valid():
            raise Http400(user_serd.errors)
        data = user_serd.data
        user = User.objects.filter(email=data["email"]).first()
        if not user:
            raise Http404(f"Username: {data['email']} not found")
        if not check_password(data["password"], user.password):
            raise Http400("Invalid password")
        return Response(
            {
                "token": JWTAPIView.get_token(user),
                "user": UserEmailSerializer(user).data,
            },
            status=status.HTTP_201_CREATED,
        )
