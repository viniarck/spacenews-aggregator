#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTTokenUserAuthentication
from rest_framework_simplejwt.tokens import RefreshToken


from spacenews_api.models.user import User


class JWTAPIView(APIView):
    """Base APIView class for JWT authentication."""

    authentication_classes = (JWTTokenUserAuthentication,)
    permission_classes = (IsAuthenticated,)

    @classmethod
    def get_token(cls, login: User) -> str:
        """Get a JWT token for a User object."""
        refresh = RefreshToken.for_user(login)
        return str(refresh.access_token)
