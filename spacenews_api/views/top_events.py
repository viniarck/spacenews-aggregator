#!/usr/bin/env python
# -*- coding: utf-8 -*-

from functools import reduce
from django.db.models import Count, Q
import rest_framework.status as status
from rest_framework.response import Response
from rest_framework.request import Request
from spacenews_api.views.auth import JWTAPIView

from spacenews_api.models.event import Event, EventSerializer
from spacenews_api.models.click import Click
from spacenews_api.exceptions import Http400


class TopEventsView(JWTAPIView):

    """TopEvents."""

    def get(self, request: Request) -> Response:
        try:
            limit = int(request.query_params.get("limit", 10))
        except ValueError as e:
            raise Http400(str(e))
        if limit < 1:
            raise Http400(f"The limit query arg has to be >= 1")
        events_total = (
            Click.objects.all()
            .values("event_id")
            .annotate(total=Count("event_id"))
            .order_by("-total")[:limit]
        )
        if not events_total:
            return Response([], status=status.HTTP_200_OK)
        qs = [Q(id=ev["event_id"]) for ev in events_total]
        event_serd = EventSerializer(
            Event.objects.filter(reduce(lambda x, y: x | y, qs)), many=True
        )
        event_serd_data = event_serd.data
        for ev_total in events_total:
            for data in event_serd_data:
                if data.get("id") == ev_total["event_id"]:
                    data["clicks"] = ev_total["total"]
                    break
        return Response(
            sorted(event_serd_data, key=lambda x: x["clicks"], reverse=True),
            status=status.HTTP_200_OK,
        )
