#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import hashlib
import json
import os
import requests
from typing import Any, Dict, List, Set, Tuple, Union
from django.core.cache import cache
from django.db import transaction
from spacenews_api.models.event import Event, EventSerializer
from spacenews_api.models.category import Category
from spacenews_api.source import Source

logger = logging.getLogger(__name__)


class Requester(Source):

    """Requester."""

    def __init__(
        self,
        source: str = "EONET",
        base_url: str = "https://eonet.sci.gsfc.nasa.gov/api/v2.1",
        api_key="",
    ) -> None:
        """Constructor of Requester."""
        super().__init__(source)
        self.base_url = base_url if base_url else os.environ.get("EONET_BASE_URL", "")
        self.api_key = api_key if api_key else os.environ.get("NASA_API_KEY", "")

    def append_if_api_key(self, url: str) -> str:
        if self.api_key:
            return f"{url}?api_key={self.append_if_api_key}"
        return url

    def _fetch_events(self) -> List[Any]:
        """Fetch events."""
        endpoint = f"{self.base_url}/events"
        endpoint = self.append_if_api_key(endpoint)
        logger.info(f"Fetching {endpoint}")
        res = requests.get(endpoint)
        return res.json()["events"]

    def _hash(self, json_obj) -> str:
        """Hash a json_obj."""
        return hashlib.sha512(json.dumps(json_obj).encode("utf-8")).hexdigest()

    def create_event(
        self, event: Dict[str, Any], *events: List[Dict[str, Any]]
    ) -> List[Event]:
        """Create an Event."""
        evs: List[Dict[str, Any]] = []
        for ev in (event, *events):
            evs.append(ev)  # type: ignore
        ev_list = self._map_eonet_event(evs)
        ev_saved: List[Event] = []
        with transaction.atomic():
            for ev_dict in ev_list:
                e: Event = ev_dict["event"]  # type: ignore
                if e:
                    e.save()
                    ev_saved.append(e)
                cats = ev_dict.get("categories", [])
                for c in cats:
                    c.event = e
                    c.save()
        return ev_saved

    def _filter_eonet_new_events(
        self, events: List[Dict[str, Any]]
    ) -> List[Dict[str, Any]]:
        """Filter EONET new events, e.g., not in the local DB yet."""
        local_events = self._ids_from_db()
        filtered = []
        for ev in events:
            if (ev["id"], self.name) not in local_events:
                filtered.append(ev)
        return filtered

    def _map_eonet_event(
        self, events: List[Dict[str, Any]]
    ) -> List[Dict[str, Union[Event, List[Category]]]]:
        """Map EONET a list of EONET events to a list of Events dicts."""
        if not events:
            return []
        evs = []
        for ev in events:
            ev_dict = {}
            e = Event(
                ext_id=ev["id"],
                source=self.name,
                date=ev["geometries"][0]["date"].split("T")[0],
                title=ev["title"],
                url=ev["sources"][0]["url"],
            )
            ev_dict["event"] = e
            cats = []
            for cat in ev.get("categories", []):
                cats.append(Category(title=cat["title"], event=e))
            ev_dict["categories"] = cats  # type:ignore
            evs.append(ev_dict)
        return evs  # type: ignore

    def _ids_from_db(self) -> Set[Tuple[int, str]]:
        evs: Set[Tuple[int, str]] = set()
        events = Event.objects.filter(source=self.name)
        if events:
            for ev in events:
                evs.add((ev.ext_id, ev.source))
        return evs

    def _fetch_update(self) -> List[Dict[str, Any]]:
        """Fetch events and update the cache if needed."""

        cached_events: List[Any] = []
        if cache.get(self.name):
            cached_events = cache.get(self.name)

        events = self._filter_eonet_new_events(self._fetch_events())
        if events:
            logger.info(f"Creating {len(events)} events")
            self.create_event(*events)  # type: ignore
        source_evs = Event.objects.filter(source=self.name)
        source_evs_serd = EventSerializer(source_evs, many=True).data
        cache.delete(self.name)
        cache.set(self.name, source_evs_serd, timeout=3600)
        cached_events = source_evs_serd
        return cached_events

    def list_events(  # type: ignore
        self, offset=0, limit: int = 100000
    ) -> List[Dict[str, Any]]:
        """List events."""
        events: List[Dict[str, Any]] = cache.get(self.name)
        if not events:
            events = self._fetch_update()
        if limit >= 0 and offset >= 0:
            return events[offset:limit]
        return events
