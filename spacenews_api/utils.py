#!/usr/bin/env python
# -*- coding: utf-8 -*-


from rest_framework.request import Request
from typing import Dict, Any
import json
from rest_framework.exceptions import ParseError
import rest_framework.status as status

from spacenews_api.exceptions import Http403


def json_or_raise(request: Request) -> Dict[str, Any]:
    """Either get json data or raise."""
    try:
        data = request.data
        if isinstance(data, dict):
            return data
        return json.loads(data)
    except (json.JSONDecodeError, TypeError) as e:
        raise ParseError(
            detail=f"The body of the request isn't valid JSON. Exception {str(e)}",
            code=status.HTTP_400_BAD_REQUEST,
        )


def has_permission_or_raise(request: Request, user_email: str) -> object:
    """Validate requests user permission on a object level."""

    username = request._user.username
    if username != user_email:
        raise Http403(
            f"User {username} doesn't have permission to access {user_email}'s resources "  # noqa
        )
    return username
