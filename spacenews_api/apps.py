from django.apps import AppConfig


class SpacenewsApiConfig(AppConfig):
    name = 'spacenews_api'
