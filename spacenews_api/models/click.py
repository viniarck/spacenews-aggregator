#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from model_utils.models import TimeStampedModel
from django.contrib.auth.models import User
import rest_framework.serializers as serializers

from spacenews_api.mixins import ValidateOnSaveMixin
from spacenews_api.models.event import Event


class Click(ValidateOnSaveMixin, TimeStampedModel):

    """Click active record."""

    id = models.AutoField(primary_key=True)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __repr__(self) -> str:
        """repr."""
        return f"Click({self.id}, {self.event_id}, {self.user_id})"


class ClickSerializer(serializers.ModelSerializer):

    """ClickSerializer."""

    class Meta:
        """Meta."""

        model = Click
        fields = ("id", "event", "user")


class ClickInSerializer(serializers.Serializer):

    event = serializers.CharField(required=True)
    email = serializers.CharField(required=True)
