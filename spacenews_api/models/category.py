#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from model_utils.models import TimeStampedModel
import rest_framework.serializers as serializers

from spacenews_api.mixins import ValidateOnSaveMixin
from spacenews_api.models.event import Event


class Category(ValidateOnSaveMixin, TimeStampedModel):

    """Category active record."""

    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, blank=True, null=True)

    def __repr__(self) -> str:
        """repr."""
        return f"Category({self.id}, {self.title}, {self.event})"

    class Meta:
        """Meta."""

        unique_together = ("title", "event")


class CategorySerializer(serializers.ModelSerializer):

    """CategorySerializer."""

    class Meta:
        """Meta."""

        model = Category
        fields = ("id", "title", "event")
