#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from model_utils.models import TimeStampedModel
import rest_framework.serializers as serializers

from spacenews_api.mixins import ValidateOnSaveMixin


class Event(ValidateOnSaveMixin, TimeStampedModel):

    """Event active record."""

    id = models.AutoField(primary_key=True)
    ext_id = models.CharField(max_length=255)
    source = models.CharField(max_length=255)
    date = models.DateField("%Y-%m-%d", null=True, blank=True)
    title = models.CharField(max_length=255, null=True, blank=True)
    url = models.URLField(blank=True, null=True)

    def __repr__(self) -> str:
        """repr."""
        return f"Event({self.id}, {self.ext_id}, {self.source}, {self.title})"

    class Meta:
        """Meta."""

        unique_together = ("ext_id", "source")


class EventSerializer(serializers.ModelSerializer):

    """EventSerializer."""

    categories = serializers.SerializerMethodField()

    def get_categories(self, event: Event):
        from spacenews_api.models.category import Category, CategorySerializer
        return CategorySerializer(Category.objects.filter(event=event), many=True).data

    class Meta:
        """Meta."""

        model = Event
        fields = ("id", "ext_id", "source", "date", "title", "url", "categories")
