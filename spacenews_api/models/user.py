#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
import rest_framework.serializers as serializers


class UserSerializer(serializers.ModelSerializer):

    """UserSerializer."""

    class Meta:
        """Meta."""

        model = User
        fields = ("email", "password")


class UserEmailSerializer(serializers.ModelSerializer):

    """UserEmailSerializer."""

    class Meta:
        """Meta."""

        model = User
        fields = ("email",)


class UserSignInSerializer(serializers.Serializer):

    """UserSignInSerializer."""

    token = serializers.CharField()
    user = UserEmailSerializer()
