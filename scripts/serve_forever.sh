#!/bin/sh

if [ -z "$GUNICORN_HTTPS" ]; then
  echo "Starting gunicorn over HTTP..."
  gunicorn -w "$GUNICORN_WORKERS" -b 0.0.0.0:8000 spacenews_site.wsgi
fi
