[![pipeline status](https://gitlab.com/viniarck/spacenews-aggregator/badges/master/pipeline.svg)](https://gitlab.com/viniarck/spacenews-aggregator/commits/master)

## 🌌 spacenews-aggregator

🌌 spacenews-aggregator is an app which aggregates, lists, and recommends spacenews events by leveraging [NASA EONET API](https://eonet.sci.gsfc.nasa.gov/docs/v2.1).

## App Architecture

The following high-level diagram illustrates the containers and services that compose this app:

![arch](./docs/arch.png)

## DB UML

![db-uml](./docs/db_uml.png)
