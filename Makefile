PROJECT_FILES := $(shell find spacenews_api spacenews_site tests -name "*.py" -not -path "spacenews_api/migrations/*")

lint:
	@echo "Running flake8 ..."
	@echo $(PROJECT_FILES) | xargs flake8 --max-line-length=88

	@echo "Running mypy ..."
	@echo $(PROJECT_FILES) | mypy --check-untyped-defs --ignore-missing-imports --show-column-numbers spacenews_api

	@echo "Running pycodestyle ..."
	@echo $(PROJECT_FILES) | xargs pycodestyle --show-pep8 --show-source --max-line-length=88

fmt:
	@echo "Running black ..."
	@echo $(PROJECT_FILES) | xargs black
	@echo "Running isort ..."
	@echo $(PROJECT_FILES) | xargs isort

develop:
	@pip install -e .[dev]

test:
	python -m pytest tests/ -vv

coverage_term:
	python -m pytest tests/ -vv --cov=spacenews_api --cov-report=term tests/

coverage_html:
	python -m pytest tests/ -vv --cov=spacenews_api --cov-report=html tests/

compose_up:
	docker-compose -f docker-compose.yml -f docker-compose.dev.yml -f docker-compose.secrets.yml up -d

compose_down:
	docker-compose down

compose_downv:
	docker-compose down -v
